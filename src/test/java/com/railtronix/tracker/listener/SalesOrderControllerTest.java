package com.railtronix.ws.controllers;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.railtronix.ws.objects.CreateSalesOrderRequest;
import com.railtronix.ws.objects.CreateSalesOrderResponse;
import com.railtronix.ws.patterns.Detail;
import com.railtronix.ws.patterns.Header;
import com.railtronix.ws.patterns.Order;
import com.railtronix.ws.patterns.OrderHeader;
import com.railtronix.ws.exceptions.BadRequestException;

@RunWith(MockitoJUnitRunner.class)
public class SalesOrderControllerTest {
	
	@InjectMocks
	SalesOrderController controller;
	
	@Test
	public void sendValidSaleOrderRequestPost(){
		CreateSalesOrderResponse resp = controller.createSalesOrder(preparateRequest());
		assertEquals(CreateSalesOrderResponse.class, resp.getClass());
	}
	
	@Test(expected=BadRequestException.class)
	public void sendNullSaleOrderRequestPost(){
		controller.createSalesOrder(null);
	}
	
	public CreateSalesOrderRequest preparateRequest(){
		CreateSalesOrderRequest request = new CreateSalesOrderRequest();
		Header header = new Header();
		header.setRequestDate("2016-01-20 00:00:00");
		header.setRequestID("120398120938712098312038");
		request.setHeader(header);
		Order order = new Order();
		OrderHeader orderHeader = new OrderHeader();
		order.setHeader(orderHeader);
		Detail detail = new Detail();
		detail.setAction("Create");
		detail.setCreated("2016-01-20 00:00:00");
		detail.setGradeItem("Item");
		detail.setLastUpdated("2016-01-20 00:00:00");
		detail.setLineNumber("123");
		detail.setQuantity("1000");
		detail.setRequestDate("2016-01-20 00:00:00");
		detail.setStatus("Active");
		detail.setUOM("TN");
		Detail[] details = new Detail[]{ detail};
		order.setDetail(details);
		request.setOrder(order);
		return request;
	}
}
